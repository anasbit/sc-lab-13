package P1;

import java.util.LinkedList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Management {
	
	private Lock lm;
	private Condition cm;
	private LinkedList<Integer> link;

	
	public Management() {
		lm = new ReentrantLock();
		cm = lm.newCondition();
		link = new LinkedList<Integer>();
	}
	
	
	public void add() {
		lm.lock();
		try {
			link.add(1);
			System.out.println("Element add in LinkedList");
			cm.signalAll();
		}
		finally {
			lm.unlock();
		}
	}
	
	public void Del() throws InterruptedException {
		lm.lock();
		try {
			while(link.size()<1){
				cm.await();

			}
			link.removeLast();
			System.out.println("Element remove in LinkedList");
				
		}
		finally {
			lm.unlock();
		}
	}

	
}
