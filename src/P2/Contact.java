package P2;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Contact {
	private Lock l ;
	private Condition cond;
	private Queue q;

	public Contact() {
		l  = new ReentrantLock();
		cond = l.newCondition();
		q = new Queue(10);
	}

	public void add(String str) throws InterruptedException {
		try {
			l .lock();

			while (q.getSize() > 10) {
				cond.await();
			}
			q.add(str);

			System.out.print("Add " + str + "\n");
			System.out.println(q.toString());
			cond.signalAll();
		} 
		finally {
			l .unlock();
		}
	}

	public void remove(String str) throws InterruptedException {
		try {
			l .lock();
			while (q.getSize() <= 0) {
				cond.await();
			}
			q.remove(0);
			System.out.print("remove " + str+"\n");
			System.out.println(q.toString());
			cond.signalAll();
		} 
		finally {
			l .unlock();
		}
	}
}
