package P2;

import java.util.ArrayList;

public class Queue {
	private ArrayList<String> list;
	private int size;
	
	public Queue(int x){
		list = new ArrayList<String>();		
		this.size = x;
	}
	public int getSize(){
		return list.size();
	}	
	public void add(String y){
		list.add(y);
	}	
	public void remove(int z){
		list.remove(z);		
	}		
	public String toString(){
		return list.toString();
	}
}
